import React, { useEffect, useState } from "react";
import moment from "moment";
import { Container, Grid, Paper, Typography } from "@material-ui/core";
import envars from "../envars";
import { CURRENCY_PAIRS } from "../utils/constants";
import withGlobalUI from "../utils/with-global-ui";
import CurrencyDataCard from "../components/CurrencyDataCard";
import PageLoadingView from "../components/PageLoadingView/PageLoadingView";
import { TrendingUpOutlined } from "@material-ui/icons";

const DashboardPage = (props) => {
  const [loaded, setLoaded] = useState(false);
  const [data, setData] = useState([]);

  useEffect(() => {
    const eventSource = new EventSource(`${envars.currencyServiceUrl}/currencies`);

    eventSource.onmessage = (e) => {
      let parsedData = JSON.parse(e.data);
      setData(parsedData.data);
      if (!loaded) setLoaded(true);
    };

    eventSource.onerror = (error) => {
      eventSource.close();
      setLoaded(true);
      props.requestDialog({
        title: "API Error",
        text: "Realtime Data Fetching Error",
        buttons: [{ onClick: () => {} }],
      });
    };

    return () => eventSource.close();
  }, []);

  if (!loaded) {
    return <PageLoadingView />;
  }
  return (
    <Container>
      <Typography variant="h4" component="h2" gutterBottom>
        Cryptocurrency Realtime Price
      </Typography>
      <Grid container>
        {Object.keys(CURRENCY_PAIRS).map((code) => {
          let currency = data.find((item) => item.base === code);
          return (
            <Grid item xs={12} sm={6} md={4} key={`currency-card-${code}`} style={{ paddingBottom: 16 }}>
              <CurrencyDataCard name={CURRENCY_PAIRS[code]} currency={currency} />
            </Grid>
          );
        })}
      </Grid>
    </Container>
  );
};

export default withGlobalUI(DashboardPage);
