let envars;
if (process.env.REACT_ENV === 'prd') {
  envars = {
    currencyServiceUrl: "http://35.221.198.8",
  }
} else if (process.env.REACT_ENV === 'dev') {
  envars = {
    currencyServiceUrl: "http://localhost:3001",
  }
} else {
  envars = {
    currencyServiceUrl: "http://35.221.198.8",
  }
}


export default envars;