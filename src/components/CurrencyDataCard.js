import React from "react";
import { Container, Grid, Paper, Typography } from "@material-ui/core";

const CurrencyDataCard = (props) => {
  const { name, currency={} } = props;

  return (
    <Container>
      <Paper className="paper-with-padding">
        <Grid>
          <Typography variant="h4" component="h3" gutterBottom>
            {name}
          </Typography>
        </Grid>
        <Grid container style={{paddingBottom: 8}}>
          <Grid>
            <Typography variant="h6" component="h3" color="primary" style={{ fontWeight: "bold" }}>
              ${currency.price || "-"}
            </Typography>
          </Grid>
        </Grid>
        <Grid container justify="space-between" alignItems="flex-end">
          <Grid item xs={6}>
            <Typography variant="body1" component="h2" color="textSecondary">
              Volume:
            </Typography>
            <Typography variant="body1" component="h2" color="textSecondary" style={{ overflow: "hidden" }}>
              {currency.volume ? parseFloat(currency.volume).toPrecision(15) : "-"}
            </Typography>
          </Grid>
          <Grid item xs={6}>
            <Typography variant="body1" component="h2" color="textSecondary">
              Change:
            </Typography>
            <Typography variant="body1" component="h2" color={currency.change?  +currency.change> 0 ? "secondary" : "error": "textSecondary"}>
              {currency.change || "-"}
            </Typography>
          </Grid>
        </Grid>
      </Paper>
    </Container>
  );
};

export default CurrencyDataCard;
