import { withDialog } from '../containers/DialogManager';
import { withSnackbar } from '../containers/SnackbarManager';

const withGlobalUI = component => {
  return withDialog(withSnackbar(component));
};

export default withGlobalUI;
