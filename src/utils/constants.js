export const CURRENCY_PAIRS = {
  BTC: 'Bitcoin',
  ETH: 'Ether',
  LTC: 'Litecoin',
  XMR: 'Monero',
  XRP: 'Ripple',
  DOGE: 'DOGE',
  DASH: 'Dogecoin',
  MAID: 'MaidSafeeCoin',
  LSK: 'Lisk',
  SJCX: 'Storjcoin X'
}

