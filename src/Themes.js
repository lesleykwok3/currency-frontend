import { createMuiTheme } from "@material-ui/core/styles";
// import { indigo, amber, pink } from "@material-ui/core/colors";

export const primaryMain = "#ffb917";
export const secondaryMain = "#5afc03";
export const error = "#d11904";
export const themeStyle = createMuiTheme({
  palette: {
    primary: {
      main: primaryMain,
    },
    secondary: {
      main: secondaryMain,
    },
    error: {
      main: error
    },
  }
});
