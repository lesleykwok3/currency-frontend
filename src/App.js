import React, { Component } from "react";
import { connect } from "react-redux";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import { CssBaseline } from "@material-ui/core";
import "./App.css";
import withGlobalUI from "./utils/with-global-ui";
// import api from './utils/api';
// import envars from './envars';

import SnackbarManager from "./containers/SnackbarManager";
import DialogManager from "./containers/DialogManager";
import PageLoadingView from "./components/PageLoadingView/PageLoadingView";

import DashboardPage from "./pages/DashboardPage";
// import ExportDataPage from './pages/ExportDataPage/ExportDataPage';

class App extends Component {
  state = {
    width: 0,
    height: 0,
    initAppDone: false,
    mobileShowMenu: false,
  };

  componentDidMount = () => {
    this.updateWindowDimensions();
    window.addEventListener("resize", this.updateWindowDimensions);
    this.props.initApp();
    this.setState({ initAppDone: true });
  };

  componentWillUnmount = () => {
    window.removeEventListener("resize", this.updateWindowDimensions);
  };

  updateWindowDimensions = () => {
    this.setState({ width: window.innerWidth, height: window.innerHeight });
  };

  render() {
    if (!this.state.initAppDone) {
      return (
        <div style={{ width: this.state.width, height: this.state.height }}>
          <CssBaseline />
          <PageLoadingView />
          <SnackbarManager />
          <DialogManager />
        </div>
      );
    }

    return (
      <BrowserRouter>
        <div className="App">
          <CssBaseline />
          <main className="page-container">
            <Switch>
              <Route path="/" component={DashboardPage} exact />
            </Switch>
          </main>
          <SnackbarManager />
          <DialogManager />
        </div>
      </BrowserRouter>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    snackbarManager: state.system.snackbarManager,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    initApp: () =>
      dispatch({
        type: "INIT_APP",
        payload: {},
      }),
  };
};

export default withGlobalUI(connect(mapStateToProps, mapDispatchToProps)(App));
