## Demo

http://34.80.123.122/

the application demo page

## Architecture
[![architecture](https://storage.googleapis.com/finddoc-code-test/finddoc.png "architecture")](https://storage.googleapis.com/finddoc-code-test/finddoc.png "architecture")

## Available Scripts

In the project directory, you can run:

### `npm install`

Install the dependency

### `npm run start-prd`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

### `npm run start-dev`

Launches the test runner in the interactive watch mode.<br />